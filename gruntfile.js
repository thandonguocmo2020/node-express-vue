var webpack = require("webpack");
var nodeExternals = require('webpack-node-externals');

module.exports = function (grunt) {

    var env = grunt.option('env') || 'dev';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            build: ['./dist']
        },


        watch: {
            vue: {
                files: ['**/*.vue', '**/www/**/*.js'],
                tasks: ['webpack:client'],
                options: {
                    spawn: false,
                },
            },

            cssmin: {
                files: ['**/*.css'],
                tasks: ['cssmin'],
                options: {
                    spawn: false,
                },
            },
            image: {
                files: ['**/*.{png,jpg,gif,jpeg,ico}'],
                tasks: ['imagemin'],
                options: {
                    spawn: false,
                },
            },
            html: {
                files: ['**/*.{htm,html}'],
                tasks: ['htmlmin'],
                options: {
                    spawn: false,
                },
            },
            server: {
                files: ['**/server/**/*.js'],
                tasks: ['webpack:server'],
                options: {
                    spawn: false,
                },
            },
        },

        htmlmin: {
            build: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: './src/',
                    src: '**/*.{html,htm}',
                    dest: './dist/'
                }]
            }
        },

        cssmin: {
            build: {
                files: [{
                    expand: true,
                    cwd: 'src/www',
                    //src: ['*.css', '!*.min.css'],
                    src: '**/*.css',
                    dest: './dist/www/'
                }]
            }
        },

        imagemin: {
            build: {
                options: {
                    optimizationLevel: 5
                },
                files: [{
                    expand: true,
                    cwd: 'src/www',
                    src: ['**/*.{png,jpg,gif,jpeg,ico}'],
                    dest: 'dist/www'
                }]
            }
        },


        // copy: {
        //     main: {
        //         expand: true,
        //         cwd: 'src/',
        //         src: '**',
        //         dest: 'dest/',
        //         flatten: true,
        //         filter: 'isFile',
        //     },
        // },



        // webpack

        webpack: {
            client: {
                entry: {
                    'www/js/app': './src/www/app.js',
                    //'www/css/layout': './src/www/css/layout.css'
                    //'server': './src/server/app.js',
                },
                output: {
                    //path: path.resolve(__dirname, './dist'),
                    path: "./dist/",
                    //publicPath: '/dist/',
                    filename: '[name].js'
                },
                module: {
                    rules: [{
                            test: /\.vue$/,
                            loader: 'vue-loader',
                            options: {
                                loaders: {
                                    // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                                    // the "scss" and "sass" values for the lang attribute to the right configs here.
                                    // other preprocessors should work out of the box, no loader config like this nessessary.
                                    'scss': 'vue-style-loader!css-loader!sass-loader',
                                    'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                                }
                                // other vue-loader options go here
                            }
                        },
                        {
                            test: /\.js$/,
                            loader: 'babel-loader',
                            exclude: /node_modules/
                        },
                        // Allow loading of JSON files
                        // {
                        //     test: /\.json$/,
                        //     loader: "json",
                        // },
                        //{test: /\.json$/, loader: "json"}
                        {test: /\.json$/, loader: 'json-loader'},
                        {
                            test: /\.css$/,
                            use: ['style-loader', 'css-loader']
                        },

                        {
                            test: /\.(png|jpg|gif|svg)$/,
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]?[hash]'
                            }
                        }
                    ]
                },
                performance: {
                    hints: false
                },
                //devtool: 'source-map',
                devtool: '#source-map',
                plugins: [
                    new webpack.optimize.UglifyJsPlugin({ minimize: true }),
                    // new webpack.DefinePlugin({
                    //     'process.env': { NODE_ENV: JSON.stringify("production") }
                    // }),
                    new webpack.DefinePlugin({
                        'process.env': { NODE_ENV: JSON.stringify(env) }
                    }),
                    new webpack.LoaderOptionsPlugin({
                        minimize: true
                    })
                ]
            }, //end client

            server: {
                target: 'node',
                entry: {
                    //'client/app': './src/client/main.js',
                    'app': './src/server/app.js',

                },
                output: {
                    path: "./dist/",
                    //publicPath: '/dist/',
                    filename: '[name].js'
                },
                module: {
                    rules: [{
                            test: /\.js$/,
                            loader: 'babel-loader',
                            exclude: /node_modules/
                        }

                    ]
                },
                //externals: /^[a-z][a-z\.\-0-9]*$/,
                node: {
                    // console: false,
                    // global: true,
                    // process: true,
                    // Buffer: true,
                    // __filename: false,
                    __dirname: false,
                    setImmediate: true
                },
                externals: [nodeExternals()],
                performance: {
                    hints: false
                },
                //https://webpack.github.io/docs/configuration.html#devtool
                devtool: '#source-map',
                plugins: [
                    new webpack.optimize.UglifyJsPlugin({
                        compress: true,
                        // minimize: true
                    }),
                    // new webpack.optimize.CommonsChunkPlugin({
                    //     name: 'inline',
                    //     filename: 'inline.js',
                    //     minChunks: Infinity
                    // }),
                    // new webpack.optimize.AggressiveSplittingPlugin({
                    //     minSize: 5000,
                    //     maxSize: 10000
                    // }),
                    new webpack.NoErrorsPlugin(),
                    new webpack.DefinePlugin({
                        'process.env': { NODE_ENV: JSON.stringify(env) }
                    }),
                    //new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"ignore", /* filename= */"server.bundle.js")
                    new webpack.LoaderOptionsPlugin({
                        minimize: true
                    })
                ]

            }
        }, // end webpack


    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-webpack');

    // Default task(s).
    //grunt.registerTask('default', ['clean', 'uglify', 'requirejs:client', 'htmlmin', 'cssmin', 'imagemin']);
    grunt.registerTask('default', ['clean', 'webpack', 'htmlmin', 'cssmin', 'imagemin']);

};
