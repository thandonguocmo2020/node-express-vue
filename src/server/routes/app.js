// Invoke 'strict' JavaScript mode
'use strict';

var path = require('path');

// Define the routes module' method
module.exports = function (app) {

    app.get('/*', function (req, res) {
        res.setHeader('content-type', 'text/html');
        return res.sendFile("index.html", { root: path.join(__dirname, 'www') });
    });
};
