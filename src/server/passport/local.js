// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    Member = require('../models/member.js');

// Create the Local strategy configuration method
module.exports = function () {
    // Use the Passport's Local strategy
    passport.use(
        new LocalStrategy(
            {
                // set the field name here
                usernameField: 'uid',
                passwordField: 'pwd'
            },
            function (username, password, done) {
                // Use the 'User' model 'findOne' method to find a user with the current username
                Member.findOne({ $or: [{ 'uid': username }, { 'mail': username }] })
                    //.select({_id: 1, fn:1, ln:1, uid: 1, token: 1, mail: 1, stt: 1})
                    .exec(function (err, user) {
                        // If an error occurs continue to the next middleware
                        if (err) {
                            return done(err);
                        }

                        // If a user was not found, continue to the next middleware with an error message
                        if (!user) {
                            return done(null, false, {
                                message: 'UserId or Password is invalid'
                            });
                        }

                        if (!user.authenticate(password)) {
                            return done(null, false, {
                                message: 'Invalid password'
                            });
                        }

                        if (!user.stt || user.stt <= 0) {
                            return done(null, false, {
                                message: 'Member need to activate', stt: 0
                            });
                        }

                        // Otherwise, continue to the next middleware with the user object
                        return done(null, user);
                    });
            }));
};
