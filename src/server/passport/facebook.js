// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var passport = require('passport'),
  FacebookStrategy = require('passport-facebook').Strategy,
  Channel = require('../../models/channel.js'),
  config = require('../config.js'),
  logger = require('../logger.js');

// Create the Facebook strategy configuration method
module.exports = function () {
  // Use the Passport's Facebook strategy
  passport.use(new FacebookStrategy({
    clientID: config.facebook.clientID,
    clientSecret: config.facebook.clientSecret,
    callbackURL: config.facebook.callbackURL,
    profileFields: ['id', 'about', 'name', 'displayName', 'email'],
    enableProof: true

  },
    function (req, token, refreshToken, profile, done) {


      // make the code asynchronous
      // User.findOne won't fire until we have all our data back from Facebook
      process.nextTick(function () {

        Member.findOne({ 'fb.id': profile.id },
          function (err, user) {
            // if there is an error, stop everything and return that
            // ie an error connecting to the database
            if (err)
              return done(err);

            // if the user is found then log them in
            if (user) {
              user.fb.token = token;
              user.fb.uid = profile.username;
              user.fb.nm = profile.displayName;
              user.fb.emails = profile.emails;
              user.tw.avt = 'https://graph.facebook.com/' + profile.id + '/picture?type=square';
              user.save(function (err) {
                if (err)
                  throw err;
                return done(null, user); // user found, return that user
              });

            } else {
              // if there is no user, create them
              var obj = new Member();

              // set all of the user data that we need
              obj.fb.id = profile.id;
              obj.fb.token = token;
              obj.fb.uid = profile.username;
              obj.fb.nm = profile.displayName;
              obj.fb.emails = profile.emails;
              obj.mail = profile.emails[0].value;
              obj.fn = profile.name.givenName;
              obj.ln = profile.name.familyName;
              obj.avt = 'https://graph.facebook.com/' + profile.id + '/picture?type=square';

              // save our user into the database
              obj.save(function (err) {
                if (err)
                  throw err;
                return done(null, obj);
              });
            }
          });
      }); //end 



    }
  ));
};
