var mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate'),
    crypto = require('crypto');

var memberSchema = new mongoose.Schema({
    uid: {
        type: String,
        index: true,
        unique: true
    },
    mail: {
        type: String,
        index: true,
        unique: true,
        match: [/.+\@.+\..+/, "Please fill a valid e-mail address"]
    },
    pwd: {
        type: String,
        validate: [
            function (password) {
                return password && password.length > 6;
            }, 'Password should be longer 6 char'
        ]
    },
    fn: String,
    ln: String,
    bth: Date,
    gder: Boolean,
    //avata
    avt: {
        type: String,
        default: "https://i.imgur.com/ffPLlPe.jpg"
    },
    //cover
    cver: {
        type: String,
        default: "https://i.imgur.com/z9Jhg8z.jpg"
    },
    //website   
    www: String,
    bio: String,
    rpwt: String, //resetPasswordToken
    rpwe: Date,	  //resetPasswordExpires
    role: Number,			//1admin,0member
    // twitter
    tw: {},
    // facebook
    fb: {},
    // github
    git: {},
    // google
    gg: {},
    stt: {
        type: Number,
        default: 0
    },
    onl: {
        type: Boolean,
        default: false
    },
    // last action
    act: {
        date: { type: Date, default: Date.now() },
        ip: String,
        // userAgent
        ua: String
    },
    crt: {
        type: Date,
        default: Date.now()
    }

}, {
        collection: 'members'
    }).plugin(mongoosePaginate);


memberSchema.methods.hashPassword = function (password) {
    //return hash.generate(password);
    return crypto.createHash('sha256').update(password).digest('hex');
};
memberSchema.methods.authenticate = function (password) {
    return this.hashPassword(password) == this.pwd;
};

module.exports = mongoose.model('member', memberSchema);
