// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'development' environment configuration object
module.exports = {
    port: {
        www: 8081
    },
    db: 'mongodb://localhost/blog',
    secret: '4kh8gz3dgzg6k5g5',
    log: {
        collection: 'logs',
        level: 'all'
    },

    redis: {
        host: 'localhost',
        port: 6379,
        compress: true
    },

    aws: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        region: 'us-east-1',
        s3: {
            bucket: '',
            acl: 'public-read',
        }
    },



    facebook: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        callbackURL: 'http://127.0.0.1:1337/auth/facebook/callback'
    },
    twitter: {
        consumerKey: 'get_your_own',
        consumerSecret: 'get_your_own',
        callbackURL: "http://127.0.0.1:1337/auth/twitter/callback"
    },
    github: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        callbackURL: "http://127.0.0.1:1337/auth/github/callback"
    },
    google: {
        consumerKey: 'get_your_own',
        consumerSecret: 'get_your_own',
        callbackURL: 'http://127.0.0.1:1337/auth/google/callback'
    },
    instagram: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        callbackURL: 'http://127.0.0.1:1337/auth/instagram/callback'
    }
};
