import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Index from '../views/index.vue'
import P404 from '../views/404.vue'
import Detail from '../views/post/detail.vue'
import Search from '../views/post/search.vue'

const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: [

    { path: '/p/:id', name:'post-detail', component: Detail },
    { path: '/group/:id', name:'group',component: Search },
    { path: '/', component: Index },
    { path: '*', component: P404 }
  ]
})

export default router
