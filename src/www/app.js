import Vue from 'vue';
import App from './app.vue';
import store from './store';
import router from './router';
// import Resource from 'vue-resource'
import { sync } from 'vuex-router-sync';
import Cookie from 'vue-cookie';
import Analytics from 'vue-analytics';
import NProgress from 'vue-nprogress'
// import Bootstrap from 'bootstrap-vue';

//import * as filters from './filters'

// sync the router with the vuex store.
// this registers `store.state.route`
sync(store, router);


// Vue.use(Resource)

// Globally register bootstrap-vue components
// Vue.use(Bootstrap);

Vue.use(Cookie);

Vue.use(NProgress, {
  //latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
  router: true, // Show progressbar when navigating routes, default: true
  http: true // Show progressbar when doing Vue.http, default: true
});

//Vue.use(VueAnalytics, { 'ga-id', router });

// // register global utility filters.
// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })

// router.beforeEach((to, from, next) => {
//   if (to.meta.auth && !store.state.auth.status) {
//     console.log(store.state.auth.status);
//     next({ path: '/login' })
//   } else {
//     next()
//   }
// });

// create the app instance.
// here we inject the router and store to all child components,
// making them available everywhere as `this.$router` and `this.$store`.
const app = new Vue({
    el: '#app',
    router,
    store,
    // ready:()=>{
    //   [types.REQUEST_AUTH]();
    // },
    render: h => h(App)
})

// expose the app, the router and the store.
// note we are not mounting the app here, since bootstrapping will be
// different depending on whether we are in a browser or on the server.
export { app, router, store }
