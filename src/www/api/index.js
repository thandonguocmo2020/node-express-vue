import Vue from 'vue';
import Resource from 'vue-resource';
import config from '../config';
Vue.use(Resource);

Vue.http.options.root = config.BASE_API_URL;
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;

export default {
    getToken(o) {
        return Vue.http.post([config.BASE_API_URL, '/me'].join(''));
    },

    memberInfo(data) {
        return Vue.http.post([config.BASE_API_URL, '/member/info'].join(''), data);
    },

    logout() {
        return Vue.http.get('/logout');
    },

    ///
    getPosts(o) {
        return Vue.http.post([config.BASE_API_URL, '/post/fetch'].join(''), o);
    },
    getPostDetail(o) {
        return Vue.http.post([config.BASE_API_URL, '/post/info'].join(''), o)
    },

    upload(o) {
        return Vue.http.post('/upload', o);
    }

};
