import api from '../../api';
import router from '../../router';
import { MEMBER_LOGIN, MEMBER_REGISTER, MEMBER_FORGETPWD, MEMBER_LOGOUT, MEMBER_INFO, MEMBER_RESEND, REQUEST_AUTH, REQUEST_AUTH_SUCCESS } from '../types';

export default {
    state: {
        info: {},
        user: {},
        status: false,
        token: ''
    },
    mutations: {
        [MEMBER_INFO](state, data) {
            state.info = data.user;
        },
        [MEMBER_RESEND](state, boolean) {
            state.hidden = boolean;
        },
        [REQUEST_AUTH_SUCCESS](state, data) {
            state.token = data.token
            state.user = data.user
            state.status = data.authenticated;
            // console.log(JSON.stringify(data));
        }
    },
  
    actions: {
        [REQUEST_AUTH](context, data) {
            return api.getToken(data).then(response => {
                context.commit(REQUEST_AUTH_SUCCESS, response.body)
            })
        },
        [MEMBER_LOGIN]({commit}, payload) {
            let _fv = $('.login').validate().form();
            if (_fv) {
                api.getLogin(payload).then(res => {
                    if (res.status === 200) {
                        let data = res.data
                        let user = res.data.user;
                        if (user) {
                            commit(REQUEST_AUTH_SUCCESS, data)
                            router.go(-1)
                        } else if (res.data.stt == 0) {
                            $.Msg.show(res.data.message);
                            commit(MEMBER_RESEND, false)
                        } else {
                            $.Msg.show(res.data.message);
                        }
                    }
                }, error => {
                    console.log(error)
                })
            }
        },
        [MEMBER_INFO]({commit}, payload) {
            api.memberInfo(payload).then(res => {
                commit(MEMBER_INFO, res.body);
            });
        },
        [MEMBER_REGISTER]({commit}, payload) {
            let _fv = $('.login').validate().form();
            if (_fv) {
                api.getRegister(payload).then(res => {
                    if (res.status === 200) {
                        $.Msg.show(res.data.msg);
                        if (res.data.msg !== 'E-mail already exists') {
                            router.push('/login');
                        }
                    }
                }, error => {
                    console.log(error)
                })
            }
        },
        [MEMBER_FORGETPWD]({commit}, mail) {
            let _fv = $('.login').validate().form();
            if (_fv) {
                api.postForgetpwd({ mail: mail }).then(res => {
                    if (res.status === 200) {
                        $.Msg.show(res.data.msg);
                        if (res.data.msg !== 'This E-mail is not registered !') {
                            router.push('/login')
                        }
                    }
                }, error => { console.log(error) })
            }
        },
        [MEMBER_RESEND]({commit}, mail) {
            let _fv = $('.resend').validate().form();
            if (_fv) {
                api.postResend({ uid: mail }).then(res => {
                    if (res.status === 200) {
                        $.Msg.show(res.data.msg);
                        if (res.data.msg !== 'This E-mail is not registered !') {
                            commit(MEMBER_RESEND, true)
                        }
                    }
                }, error => { console.log(error) })
            }
        },
        [MEMBER_LOGOUT]({commit}) {
            api.getLogout();
            router.push('/')
        },
    },
}
