import api from '../../api'
import _ from 'underscore'
import { POST_GET_LIST_SUCCESS, POST_GET_DETAIL, POST_GET_LIST, POST_GET_DETAIL_SUCCESS, POST_CLEAN, POST_CLEAN_SUCCESS } from '../types'

export default {
    state: {
        docs: [],
        page: 1,
        info: null,
        suggest: [],
        recommended: []
    },

    mutations: {
        [POST_GET_LIST_SUCCESS](state, data) {

            state.docs = state.docs.concat(data.docs);
            state.pages = data.pages;
            if (state.page <= data.pages) {
                state.page = state.page + 1
            }
        },
        [POST_GET_DETAIL_SUCCESS](state, data) {
            console.log(data);
            state.info = data.info;
            state.suggest = data.suggest;
            state.recommended = data.recommended;

        },
        [POST_CLEAN_SUCCESS](state) {
            state.docs = [];
            state.page = 1;
            state.suggest = [];
            state.recommended = [];

        }

    },
    getters: {
        docs: state => state.docs,
        page: state => state.page,
        info: state => state.info,
        suggest: state => state.suggest,
        recommended: state => state.recommended
    },

    actions: {
        [POST_GET_LIST]({ commit }, payload) {
            return api.getPosts(payload).then(res => {
                commit(POST_GET_LIST_SUCCESS, res.data);
            })
        },
        [POST_GET_DETAIL]({ commit }, payload) {
            return api.getPostDetail(payload).then(res => {
                commit(POST_GET_DETAIL_SUCCESS, res.data);
            });
        },
        [POST_CLEAN]({ commit }, payload) {
            commit(POST_CLEAN_SUCCESS);
        }

    }
}
